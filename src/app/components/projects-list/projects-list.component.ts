import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss']
})
export class ProjectsListComponent implements OnInit {

  constructor() { }

  projects = [
    {
      title: 'smith office project',
      location: 'Dubai, UAE',
      image: 'https://picsum.photos/id/896/520/520',
    },
    {
      title: 'Palomar Hospitala',
      location: 'Dubai, UAE',
      image: 'https://picsum.photos/id/795/520/520',
    },
    {
      title: 'Jones Hightschool Estimate',
      location: 'Dubai, UAE',
      image: 'https://picsum.photos/id/794/520/520',
    },
    {
      title: 'smith office project',
      location: 'Dubai, UAE',
      image: 'https://picsum.photos/id/893/520/520',
    },
    {
      title: 'Palomar Hospitala',
      location: 'Dubai, UAE',
      image: 'https://picsum.photos/id/790/520/520',
    },
    {
      title: 'Jones Hightschool Estimate',
      location: 'Dubai, UAE',
      image: 'https://picsum.photos/id/791/520/520',
    },
    {
      title: 'smith office project',
      location: 'Dubai, UAE',
      image: 'https://picsum.photos/id/881/520/520',
    },
    {
      title: 'Palomar Hospitala',
      location: 'Dubai, UAE',
      image: 'https://picsum.photos/id/782/520/520',
    },
    {
      title: 'Jones Hightschool Estimate',
      location: 'Dubai, UAE',
      image: 'https://picsum.photos/id/783/520/520',
    },
  ]

  ngOnInit() {
  }

}
