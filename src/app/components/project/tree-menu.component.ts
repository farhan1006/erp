import { NestedTreeControl } from '@angular/cdk/tree';
import { Component } from '@angular/core';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { BehaviorSubject, Observable, of as observableOf } from 'rxjs';

export class FileNode {
  children: FileNode[];
  filename: string;
  type: any;
}

@Component({
  selector: 'app-tree-menu',
  templateUrl: './tree-menu.component.html',
  styleUrls: ['./tree-menu.component.scss']
})
export class TreeMenuComponent {

  nestedTreeControl: NestedTreeControl<FileNode>;
  nestedDataSource: MatTreeNestedDataSource<FileNode>;
  dataChange: BehaviorSubject<FileNode[]> = new BehaviorSubject<FileNode[]>([]);

  constructor() {
    this.nestedTreeControl = new NestedTreeControl<FileNode>(this._getChildren);
    this.nestedDataSource = new MatTreeNestedDataSource();

    this.dataChange.subscribe(data => this.nestedDataSource.data = data);

    this.dataChange.next([
      {
        filename: "Debors Control A/C",
        type: "",
        children: [
          {
            filename: "General",
            type: "General",
            children: [],
          },
          {
            filename: "Company",
            type: "Company",
            children: [],
          },
          {
            filename: "Credit Limit",
            type: "CreditLimit",
            children: [],
          },
          {
            filename: "Documents",
            type: "Documents",
            children: [],
          },
          {
            filename: "Contact Details",
            type: "ContactDetails",
            children: [],
          },
          {
            filename: "Reference",
            type: "Reference",
            children: [],
          },
          {
            filename: "Forwarder",
            type: "Forwarder",
            children: [],
          },
          {
            filename: "Part Code",
            type: "PartCode",
            children: [],
          },
          {
            filename: "Checklist",
            type: "Checklist",
            children: [],
          },
          {
            filename: "Terms",
            type: "Terms",
            children: [],
          },
          {
            filename: "Reports",
            type: "",
            children: [
              {
                filename: "Messages",
                type: "Messages",
                children: []
              },
              {
                filename: "Statement",
                type: "Statement",
                children: []
              },
              {
                filename: "Ageing",
                type: "Ageing",
                children: []
              },
              {
                filename: "Sales Report",
                type: "SalesReport",
                children: []
              }
            ],
          },
        ],
      },
    ]);
  }

  private _getChildren = (node: FileNode) => { return observableOf(node.children); };
  hasNestedChild = (_: number, nodeData: FileNode) => { return !(nodeData.type); };

}



