import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  username = 'Mike Smith';
  useravatar = 'https://picsum.photos/id/666/80/80';

  links = [
    {
      title: 'Category',
      status: false,
    },
    {
      title: 'Accounts Tree',
      status: false,
    },
    {
      title: 'Bank Master Tree',
      status: false,
    },
    {
      title: 'Receivable Master',
      status: true,
    },
    {
      title: 'Payable Master',
      status: false,
    },
  ]

  ngOnInit() {
  }

}
