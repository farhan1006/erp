import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Matrial

import {
  MatCardModule,
  MatButtonModule,
  MatMenuModule,
  MatExpansionModule,
  MatListModule,
  MatSelectModule,
  MatFormFieldModule,
  MatTreeModule,
  MatInputModule,
  MatIconModule,
} from '@angular/material';

// Dev
import { UtilityNavComponent } from './components/utility-nav/utility-nav.component';
import { ProjectsListComponent } from './components/projects-list/projects-list.component';
import { ProjectComponent } from './components/project/project.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TreeMenuComponent } from './components/project/tree-menu.component';



@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    UtilityNavComponent,
    ProjectsListComponent,
    ProjectComponent,
    SidebarComponent,
    TreeMenuComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    BrowserAnimationsModule,
    // Mat    
    MatButtonModule,
    MatIconModule,
    MatTreeModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatMenuModule,
    MatExpansionModule,
    MatListModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
