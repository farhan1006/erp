import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'epromis';
  username = 'Mike Smith';
  useravatar = 'https://picsum.photos/id/666/80/80';
}
